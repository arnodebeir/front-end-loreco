import React, { useState, useEffect } from 'react';
import { View, Text, FlatList } from 'react-native';

import Card from '../shared/card';
import { globalStyles } from '../styles/global';
import { MANI } from 'mani-pona';

export default function Home() {
	const maniClient = global.maniClient;

	const [ contributions, setContributions ] = useState([]);
	const [ ready, setReady ] = useState(false);

	useEffect(() => {
		loadData();
	}, []);

	async function loadData() {
		await maniClient.demurageHistory.all().then((demurageHistory) => {
			setContributions(demurageHistory);
		});
		setReady(true);
	}

	if (ready === false) {
		return null;
	} else {
		return (
			<View style={globalStyles.main}>
				<FlatList
					data={contributions}
					keyExtractor={(item) => item.demurageId.toString()}
					renderItem={({ item }) => (
						<Card>
							<Text style={globalStyles.property}>{new Date(item.date).toLocaleDateString()}</Text>
							<Text style={globalStyles.price}>{MANI(item.amount).format()}</Text>
						</Card>
					)}
				/>
			</View>
		);
	}
}
